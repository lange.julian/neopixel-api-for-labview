// ================================================================================
// UDP Test
// This sketch receives UDP message strings via multicast and prints them 
// to the serial port. This code is based on Michael Margolis' public domain UDP example. 

#include <SPI.h>          // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>  // UDP library from: bjoern@cs.stanford.edu 12/30/2008


// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress local_ip(10,21,31,177);
IPAddress dns(10,21,31,10);
IPAddress gateway(10,21,31,10);
IPAddress subnet(255,255,255,0);

unsigned int localPort = 58432;             // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;
// ================================================================================


// ********************************************************************************
#include <FastLED.h>

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

// How many leds in your strip?
#define NUM_LEDS 29 

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 6
//#define CLOCK_PIN 13

// Controller-specific settings
#define LED_TYPE   WS2812
#define COLOR_ORDER   GRB

// Define the array of leds
CRGB leds[NUM_LEDS];

// Default values for color
static uint8_t r = 108;
static uint8_t g = 0;
static uint8_t b = 128;

// ********************************************************************************


void setup() {
  delay( 3000 ); //safety startup delay

  // ================================================================================
  // start the Ethernet and UDP:
  Ethernet.begin(mac,local_ip,dns,gateway,subnet);
  Udp.begin(localPort);

  Serial.begin(9600);
  // ================================================================================
  

  // ********************************************************************************
  LEDS.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds,NUM_LEDS);
  LEDS.setBrightness(84);
  // ********************************************************************************
}


void loop()
{
  
  // ********************************************************************************
  // First slide the led in one direction
  for(int i = 0; i < NUM_LEDS; i++) {
    // Check if a new color value was sent via ethernet
    checkUDP();
    // Set the i'th led to the current color
    leds[i] = CRGB(r, g, b);
    // Show the leds
    FastLED.show(); 
    // now that we've shown the leds, reset the i'th led to black
    //leds[i] = CRGB::Black;
    fadeall();
    // Wait a little bit before we loop around and do it again
    delay(10);
  }

  // Now go in the other direction.  
  for(int i = (NUM_LEDS)-1; i >= 0; i--) {
    // Check if a new color value was sent via ethernet
    checkUDP();
    // Set the i'th led to the current color
    leds[i] = CRGB(r, g, b);
    // Show the leds
    FastLED.show();
    // now that we've shown the leds, reset the i'th led to black
    //leds[i] = CRGB::Black;
    fadeall();
    // Wait a little bit before we loop around and do it again
    delay(10);
  }
  // ********************************************************************************
}


// ********************************************************************************
// scale down a RGB to N 256ths of it's current brightness, using 'plain math' dimming 
// rules, which means that if the low light levels may dim all the way to 100% black.
// ********************************************************************************
void fadeall() {
  for(int i = 0; i < NUM_LEDS; i++) { 
    // scale down to 250/256ths
    leds[i].nscale8(250); 
  } 
}

// ================================================================================
// Check if a UDP packet was sent, and if so, parse it and update the RGB variables
// ================================================================================
void checkUDP() {
  // The hue set via UDP 
  String udpString = "";
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    udpString = packetBuffer;
    r = udpString.substring(0,3).toInt();
    g = udpString.substring(4,7).toInt();
    b = udpString.substring(8,11).toInt();
    Serial.print(r);
    Serial.print("/");
    Serial.print(g);
    Serial.print("/");
    Serial.println(b);

    /*
    Serial.print(packetBuffer);

    //Serial.print("Received packet of size ");
    //Serial.println(packetSize);
    Serial.print(" (from ");
    IPAddress remote = Udp.remoteIP();
    for (int i =0; i < 4; i++)
    {
      Serial.print(remote[i], DEC);
      if (i < 3)
      {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.print(Udp.remotePort());
    Serial.println(")");
    */
  }
}
// ================================================================================
